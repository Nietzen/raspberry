package models;

public class JoystickSend {
    private Integer angle;
    private Integer strength;

    public Integer getAngle() {
        return angle;
    }

    public JoystickSend setAngle(Integer angle) {
        this.angle = angle;
        return this;
    }

    public Integer getStrength() {
        return strength;
    }

    public JoystickSend setStrength(Integer strength) {
        this.strength = strength;
        return this;
    }
}
