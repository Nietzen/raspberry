package com.nietzen.websocket;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import java.net.URISyntaxException;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.emitter.Emitter;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import io.github.controlwear.virtual.joystick.android.JoystickView;
import models.JoystickSend;


public class MainActivity extends AppCompatActivity {

    //https://socket.io/blog/native-socket-io-and-android/
    //https://github.com/controlwear/virtual-joystick-android

    private Socket mSocket;
    private JoystickView joystick;
    private final Gson gson = new Gson();
    private Boolean connect = false;
    private Button btnSpeed;
    private Boolean needSpeed = false;

    {
        try {
            this.mSocket = IO.socket(Constants.SERVER);
            Log.d("TestConnection", "connected");
            this.setConnect(true);
        } catch (URISyntaxException e) {
            this.setConnect(false);
            Log.d("TestConnection", "No connected");
        }
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (args.length < 2) return;
                    Log.d("test", String.valueOf(args.length));
                    JSONObject dataJson = (JSONObject) args[1];
                    String data;
                    String count;

                    try {
                        Log.d("JSON", dataJson.toString());
                        data = dataJson.getString("data");
                        count = dataJson.getString("count");
                    } catch (JSONException e) {
                        Log.d("error", e.getMessage());
                        return;
                    }
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        this.joystick = (JoystickView) findViewById(R.id.joystickView);
        this.btnSpeed = (Button) findViewById(R.id.btn_speed);
        this.connect();
        this.changeJoystick();
        this.setTouchListener();
    }

    protected void connect() {
        this.getmSocket().on("connected", this.onNewMessage);
        this.getmSocket().connect();
    }

    @Override
    public void onDestroy() {
        this.setConnect(false);
        super.onDestroy();

        this.mSocket.disconnect();
    }

    protected void changeJoystick() {
        this.joystick.setOnMoveListener(new JoystickView.OnMoveListener() {
            public void onMove(int angle, int strength) {
                MainActivity.this.sendAngleStrength(angle);
                Log.d("joy", "moviendo");
            }
        }, 17); // around 60/sec
    }

    public void sendAngleStrength(int angle) {
        if (!this.getConnect()) return;

        angle = 180 - angle;

        JoystickSend joystickSend = new JoystickSend();
        joystickSend.setAngle(angle).setStrength(this.getNeedSpeed() ? 1 : 0);

        try {
            JSONObject obj = new JSONObject(this.getGson().toJson(joystickSend));

            this.getmSocket().emit(Constants.SEND_MOVE, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setTouchListener() {
        this.getBtnSpeed().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                // Check if the button is PRESSED
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.d("button", "ACTION_DOWN");
                    MainActivity.this.setNeedSpeed(true);
                }// Check if the button is RELEASED
                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    Log.d("button", "ACTION_UP");
                    MainActivity.this.setNeedSpeed(false);
                }
                return false;
            }
        });
    }

    protected void sendRun() {
        this.getmSocket().emit(Constants.SEND_SPEED, Constants.SEND_RUN);
    }

    protected void sendStop() {
        this.getmSocket().emit(Constants.SEND_SPEED, Constants.SEND_STOP);
    }

    public Socket getmSocket() {
        return mSocket;
    }

    public Gson getGson() {
        return gson;
    }

    public Boolean getConnect() {
        return connect;
    }

    public void setConnect(Boolean connect) {
        this.connect = connect;
    }

    public Button getBtnSpeed() {
        return btnSpeed;
    }

    public Boolean getNeedSpeed() {
        return needSpeed;
    }

    public void setNeedSpeed(Boolean needSpeed) {
        this.needSpeed = needSpeed;
    }
}
