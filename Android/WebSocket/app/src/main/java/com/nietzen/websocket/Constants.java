package com.nietzen.websocket;

public class Constants {
    private static final String IP = "10.65.66.114";
    private static final String NAMESPACE = "/car";
    public static final String SERVER = "http://" + Constants.IP + ":8080" + Constants.NAMESPACE;

    public static final String SEND_MOVE = "send move";
    public static final String SEND_SPEED = "send speed";
    public static final String SEND_STOP = "stop";
    public static final String SEND_RUN = "run";
}
