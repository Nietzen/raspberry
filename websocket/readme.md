https://flask-socketio.readthedocs.io/en/latest/
https://aiohttp.readthedocs.io/en/stable/

install:

    - pip3 install -r requirements.txt 
    - pip3 install aiohttp
    - pip3 install cchardet
    - pip3 install aiodns
    - pip3 install flask-socketio

