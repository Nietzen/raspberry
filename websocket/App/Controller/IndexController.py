from aiohttp import web


class IndexController(object):

    @classmethod
    async def indexAction(self, request):
        with open('index.html') as f:
            return web.Response(text=f.read(), content_type='text/html')
