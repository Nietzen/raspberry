from aiohttp import web
# from App.Controller.IndexController import IndexController

from raspberry.servo import Servo
from raspberry.motor import Motor
from raspberry.ultrasonic import UltraSonic

import socketio

sio = socketio.AsyncServer()
app = web.Application()
sio.attach(app)

# indexController = IndexController()
servo = Servo()
motor = Motor()
# ultrasonic = UltraSonic()

servo.setup()
motor.setup()
# ultrasonic.setup()

ROUTING = [
    # web.get('/', indexController.indexAction)
]

app.add_routes(ROUTING)

namespace = '/car'


@sio.on('disconnect request', namespace=namespace)
async def disconnect_request(sid):
    await sio.disconnect(sid, namespace=namespace)


@sio.on('connect', namespace=namespace)
async def on_connect(sid, environ):
    print('connected')
    await sio.emit(event='connected', data={'data': 'Connected', 'count': 0}, room=sid,
                   namespace=namespace)


@sio.on('disconnect', namespace=namespace)
def on_disconnect(sid):
    print('Client disconnected')


@sio.on('send move', namespace=namespace)
async def send_move(sid, data):
    servo_move(data['angle'], data['strength'])


def servo_move(angle, strength):
    if angle == 180 and strength == 0:
        angle = 85

    servo.setDutyCycle(angle)

    if strength != 0: #and (not possible_shock()):

        motor.ahead()
    else:
        motor.stop()


def possible_shock():

    if not ultrasonic.get_process():
        return ultrasonic.getDistance() >= 4.5

    return True


app.router.add_static('/static', 'static')

if __name__ == '__main__':
    web.run_app(app)
