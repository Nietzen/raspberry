# Raspberry PI Project Car UMCA

![alt Raspberry](https://images-na.ssl-images-amazon.com/images/I/91zSu44%2B34L._SX355_.jpg)
![alt Android](https://cdn-images-1.medium.com/max/1600/1*xNQHxXBX-1RQCPM3LYa3wA.png)

The idea of this project is a car toy where the raspberry expose a websoket api where it control wth the technology you like (Android Example Code).

The web server is with aiohttp because is so cool, and python-socketio for manange the sockets

![socket IO](https://python-socketio.readthedocs.io/en/latest/_static/logo.png)
![alt aiohttp](https://aiohttp.readthedocs.io/en/stable/_static/aiohttp-icon-128x128.png)

Requirements:

    aiohttp == 3.3.2
    cchardet == 2.1.1
    aiodns == 1.1.1
    Flask-SocketIO == 3.0.1
    python-socketio == 2.0.0
    aiohttp_utils == 3.0.0

