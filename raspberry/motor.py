#!/usr/bin/python
# pip install l293d

import time
import RPi.GPIO as GPIO


class Motor:
    Motor2A = 16
    Motor2B = 18
    Motor2E = 22

    Motor1A = 31
    Motor1B = 33
    Motor1E = 35

    def setup(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.Motor1A, GPIO.OUT)
        GPIO.setup(self.Motor1B, GPIO.OUT)
        GPIO.setup(self.Motor1E, GPIO.OUT)
        GPIO.setup(self.Motor2A, GPIO.OUT)
        GPIO.setup(self.Motor2B, GPIO.OUT)
        GPIO.setup(self.Motor2E, GPIO.OUT)
        self.stop()

    def turnOnOff(self, on):
        GPIO.output(self.Motor1E, GPIO.HIGH if on else GPIO.LOW)
        GPIO.output(self.Motor2E, GPIO.HIGH if on else GPIO.LOW)

    def stop(self):
        self.turnOnOff(False)

    def ahead(self):
        GPIO.output(self.Motor1A, GPIO.HIGH)
        GPIO.output(self.Motor1B, GPIO.LOW)
        GPIO.output(self.Motor2A, GPIO.LOW)
        GPIO.output(self.Motor2B, GPIO.HIGH)
        self.turnOnOff(True)

    def behind(self):
        GPIO.output(self.Motor1B, GPIO.HIGH)
        GPIO.output(self.Motor1A, GPIO.LOW)


        GPIO.output(self.Motor2B, GPIO.LOW)
        GPIO.output(self.Motor2A, GPIO.HIGH)
        self.turnOnOff(True)

    def loop(self):
        count = 0

        self.turnOnOff(True)

        while count < 4:
            print ("ahead")
            self.ahead()
            time.sleep(0.5)
            print ("behind")
            # <ºself.behind()
            count += 1
            time.sleep(0.5)

        self.turnOnOff(False)

    def destroy(self):
        print('destroy')
        GPIO.cleanup()


if __name__ == '__main__':
    motor = Motor()

    motor.setup()
    try:
        motor.loop()
        motor.destroy()
    except KeyboardInterrupt:
        motor.destroy()
