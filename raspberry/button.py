from gpiozero import Button
import RPi.GPIO as GPIO

ledPin = 18
button = Button(2)

def setup():
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(ledPin, GPIO.OUT)

def loop():
    while (True):
        button.wait_for_active()
        GPIO.output(ledPin, GPIO.LOW)
        print("1")
        button.wait_for_inactive()
        GPIO.output(ledPin, GPIO.HIGH)
        print("0")

def destroy():
    GPIO.output(ledPin, GPIO.HIGH)
    GPIO.cleanup()
    
if __name__ == '__main__':
    setup()
   
    try:
        loop()
    except KeyboardInterrupt:
        destroy()