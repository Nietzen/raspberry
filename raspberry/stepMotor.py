#!/usr/bin/python
# Import required libraries
import sys
import time
import RPi.GPIO as GPIO


class StepMotor:
    # Define GPIO signals to use
    # Physical pins 11,15,16,18
    # GPIO17,GPIO22,GPIO23,GPIO24
    StepPins = [17, 22, 23, 24]
    # Define advanced sequence
    # as shown in manufacturers datasheet
    Seq = []
    StepCount = []
    StepDir = []
    WaitTime = 0.001
    StepCounter = 0

    def setup(self, args):
        self.Seq = [
            [1, 0, 0, 1],
            [1, 0, 0, 0],
            [1, 1, 0, 0],
            [0, 1, 0, 0],
            [0, 1, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 1, 1],
            [0, 0, 0, 1]
        ]
        # Use BCM GPIO references
        # instead of physical pin numbers
        GPIO.setmode(GPIO.BCM)

        # Set all pins as output
        for pin in self.StepPins:
            print("Setup pins")
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, False)

        self.StepCount = len(self.Seq)
        self.ahead()

        # Set to -1 or -2 for anti-clockwise

        # Read wait time from command line
        if len(args) > 1:
            self.WaitTime = int(sys.argv[1]) / float(1000)

            # Initialise variables
            self.StepCounter = 0

    def ahead(self):
        self.StepDir = 1  # Set to 1 or 2 for clockwise

    def behind(self):
        self.StepDir = -1  # Set to -1 or -2 for anti-clockwise

    def loop(self):
        print('Seq', self.Seq)
        print('StepPins', self.StepPins)
        print('StepCount', self.StepCount)
        print('StepDir', self.StepDir)
        print('StepCounter', self.StepCounter)
        print('WaitTime', self.WaitTime)

        # Start main loop
        self.ahead()
        while True:
            self.ahead()
            print(self.StepCounter),
            print(self.Seq[self.StepCounter])

            for pin in range(0, 4):

                xpin = self.StepPins[pin]

                if self.Seq[self.StepCounter][pin] != 0:
                    print("Enable GPIO %i" % xpin)
                    GPIO.output(xpin, True)
                else:
                    GPIO.output(xpin, False)

            self.StepCounter += self.StepDir

            # If we reach the end of the sequence
            # start again
            if self.StepCounter >= self.StepCount:
                self.StepCounter = 0
            if self.StepCounter < 0:
                self.StepCounter = self.StepCount + self.StepDir

            # Wait before moving on
            time.sleep(self.WaitTime)
            self.behind()

    def destroy(self):
        GPIO.cleanup()


if __name__ == '__main__':

    stepMotor = StepMotor()

    stepMotor.setup(sys.argv)
    try:
        stepMotor.loop()
    except KeyboardInterrupt:
        stepMotor.destroy()
