# Libraries
# set GPIO Pins https://tutorials-raspberrypi.com/raspberry-pi-ultrasonic-sensor-hc-sr04/
import RPi.GPIO as GPIO
import time


class UltraSonic:
    GPIO_TRIGGER = 12
    GPIO_ECHO = 32
    __proccess = False

    def setup(self):
        # GPIO Mode (BOARD / BCM)
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)

        # set GPIO direction (IN / OUT)
        GPIO.setup(self.GPIO_TRIGGER, GPIO.OUT)
        GPIO.setup(self.GPIO_ECHO, GPIO.IN)
        self.__proccess = False

    def getDistance(self):


        self.__proccess = True
        # set Trigger to HIGH
        GPIO.output(self.GPIO_TRIGGER, True)

        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(self.GPIO_TRIGGER, False)

        StartTime = time.time()
        StopTime = time.time()

        # save StartTime
        while GPIO.input(self.GPIO_ECHO) == 0:
            StartTime = time.time()

        # save time of arrival
        while GPIO.input(self.GPIO_ECHO) == 1:
            StopTime = time.time()

        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2
        self.__proccess = False

        return distance

    def get_process(self):
        return self.__proccess


if __name__ == '__main__':
    try:
        ultrasonic = UltraSonic()
        ultrasonic.setup()

        while True:
            print(ultrasonic.GPIO_TRIGGER)
            print(ultrasonic.GPIO_ECHO)
            dist = ultrasonic.getDistance()
            print("Measured Distance = %.1f cm" % dist)
            time.sleep(1)

        # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()
