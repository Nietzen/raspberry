# https://www.nociones.de/controlar-un-servo-con-rasperry-pi-usando-rpio-pwm-y-dma/

import RPi.GPIO as GPIO  # Importamos la libreria RPi.GPIO
import time  # Importamos time para poder usar time.sleep


class Servo:
    servoPin = 11 # GPI17
    centerMove = 7.5
    rightMove = 10.5
    leftMove = 4.5
    servoController = None

    def setup(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.servoPin, GPIO.OUT)
        self.servoController = GPIO.PWM(self.servoPin, 50)
        self.servoController.start(self.centerMove)

    def center(self):
        self.servoController.ChangeDutyCycle(self.centerMove)

    def right(self):
        self.servoController.ChangeDutyCycle(self.rightMove)

    def left(self):
        self.servoController.ChangeDutyCycle(self.leftMove)

    def setDutyCycle(self, angle):
        angle = self.handleAngle(angle)
        dutycycle = (((angle / 180.0) + 1.0) * 5.0)

        self.servoController.ChangeDutyCycle(dutycycle)

    def handleAngle(self, angle):
        if angle > 180 and angle < 270:
            angle -= angle - 180

        if angle >= 270:
            angle = angle - 180 - (1 + (angle - 270))

        if angle < 0:
            angle = angle * -1

        return angle

# try:
#
#     servo = Servo()
#     servo.setup()
#
#     while True:  # iniciamos un loop infinito
#         servo.center()
#         time.sleep(0.5)  # pausa de medio segundo
#         servo.right()
#         time.sleep(0.5)  # pausa de medio segundo
#         servo.center()
#         time.sleep(0.5)  # pausa de medio segundo
#         servo.left()
#         time.sleep(0.5)  # pausa de medio segundo
#
# except KeyboardInterrupt:  # Si el usuario pulsa CONTROL+C entonces...
#     p.stop()  # Detenemos el servo
#     GPIO.cleanup()
